const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();
exports.addSendingTime = functions.database.ref('/com_list/{push_id}/data')
    .onCreate((snapshot, context) => {
        console.log('in functions');
        // TODO 2: 
        // 1. get cureent date data
        // 2. add this data to new column named "time" to current node
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = (today.getHours()+8) + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;

        return snapshot.ref.parent.child('time').set(dateTime);
    });